script.on_event(defines.events.on_player_created, function(event)
    out = [[
recipes:
]]

    printProduct = function(prod)
       indent = "      "
       out = "    - " .. "type: \"" .. prod.type .. "\"\n"
       out = out .. indent .. "name: \"" .. prod.name .. "\"\n"
       if prod.amount ~= nil then
          out = out .. indent .. "amount: " .. tostring(prod.amount) .. "\n"
       else
          out = out .. indent .. "amount_min: " .. tostring(prod.amount_min) .. "\n"
          out = out .. indent .. "amount_max: " .. tostring(prod.amount_max) .. "\n"
          out = out .. indent .. "probability: " .. tostring(prod.probability) .. "\n"
       end
       return out
    end

    printIngredient = function(ing)
       indent = "      "
       out = "    - " .. "type: \"" .. ing.type .. "\"\n"
       out = out .. indent .. "name: \"" .. ing.name .. "\"\n"
       out = out .. indent .. "amount: " .. tostring(ing.amount) .. "\n"
       return out
    end

    for i, recipe in pairs(game.forces["player"].recipes) do
       out = out .. "  " .. recipe.name .. ":\n"
       --out = out .. "    localized-name: \"" .. tostring(recipe.localised_name) .. "\"\n"
       out = out .. "    enabled: " .. tostring(recipe.enabled) .. "\n"
       out = out .. "    hidden: " .. tostring(recipe.hidden) .. "\n"
       out = out .. "    group: \"" .. recipe.group.name .. "\"\n"
       out = out .. "    subgroup: \"" .. recipe.subgroup.name .. "\"\n"
       out = out .. "    energy: " .. tostring(recipe.energy) .. "\n"
       out = out .. "    products:\n"
       for k, v in ipairs(recipe.products) do
          out = out .. printProduct(v)
       end
       out = out .. "    ingredients:\n"
       for k, v in ipairs(recipe.ingredients) do
          out = out .. printIngredient(v)
       end
    end

    out = out .. "mods:\n"
    for k, v in pairs(game.active_mods) do
       out = out .. "  " .. k .. ": \"" .. v .. "\"\n"
    end

    game.write_file("datadump.yml", out)
end)
